#pragma once
#include "stdafx.h"
#include <string>


class customEx {
	std::string msg;
public:
	customEx()
		: msg("This is a custom exception!") {}
	customEx(const std::string& err)
		: msg(err) {}
	std::string what() { return msg; }
};

class itsFull {
	std::string msg;
public:
	itsFull()
		: msg("It's full!!!") {}
	itsFull(const std::string& err)
		: msg(err) {}
	std::string what() { return msg; }
};

class itsEmpty {
	std::string msg;
public:
	itsEmpty()
		: msg("It's empty!!!") {}
	itsEmpty(const std::string& err)
		: msg(err) {}
	std::string what() { return msg; }
};