#pragma once
#include <list>
#include "exceptions.h"
#include "vertex.h"

template <class V, class E>
class vertex;
template <class V, class E>
class ALgraph;


template <class V, class E>
class edge {

	
	friend class vertex<V,E>;
	friend class ALgraph<V,E>;

public:

	edge();
	edge(const E&);
	E operator*() const;
	bool isIncident( vertex<V,E>*) const;
	vertex<V,E>* opposite( vertex<V,E>*) const throw(customEx);
	bool isAdjacentTo( edge* ) const;

private:

	E element;
	vertex<V, E>* end1;
	vertex<V, E>* end2;
public: // DFS flags
	bool discovery;
	bool back;
	bool cross;

};
// CODE BELOW

template<class V, class E>
edge<V, E>::edge()
	: element(0), end1(NULL), end2(NULL), discovery(false), back(false){}

template<class V, class E>
edge<V, E>::edge(const E& inp)
	: element(inp), end1(NULL), end2(NULL), discovery(false), back(false) {}

template <class V, class E>
bool edge<V, E>::isIncident( vertex<V,E>* aVert) const {
	if (end1 == aVert || end2 == aVert) {
		return true;
	}
	else { 
		return false; 
	}
}

template<class V, class E>
E edge<V, E>::operator*() const{
	return element;
}

template<class V, class E>
vertex<V,E>* edge<V, E>::opposite( vertex<V,E>* vert) const throw(customEx){
	try {
		if (!isIncident(vert)) throw customEx("ERROR: opposite(): my arg is not incident!");
		if (end1 == vert) {
			return end2;
		}
		else {
			return end1;
		}

	}
	catch (customEx& obj) {
		std::cout << obj.what() << std::endl;
	}
}

template<class V, class E>
bool edge<V, E>::isAdjacentTo(edge* otherEdge) const{
	for (std::list<edge*>::iterator i = end1->incList.begin(); i != end1->incList.end(); ++i) {
		if (*i == otherEdge) return true;
	}
	for (std::list<edge*>::iterator i = end2->incList.begin(); i != end2->incList.end(); ++i) {
		if (*i == otherEdge) return true;
	}
	return false;
}