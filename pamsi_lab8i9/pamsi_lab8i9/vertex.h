#pragma once
#include <list>
#include "edge.h"

template <class V, class E>
class ALgraph;

template <class V, class E>
class edge;

template <class V, class E>
class vertex {
	template <typename J, typename Q>
	friend std::ostream& operator<< (std::ostream&, const ALgraph<J, Q>&);

	friend class edge<V,E>;
	friend class ALgraph<V,E>;

public:
	vertex();
	vertex(const V&);
	V operator*() const;
	//std::list<edge<V, E>*> incEdges() const;
	bool isAdjacentTo(vertex*)const ;

private:
	V element;
	std::list<edge<V, E>*> incList;
public: // DFS flags
	bool visited;
};

/**********************************************/
/***************** CODE BELOW *****************/
/**********************************************/





template <class V, class E>
V vertex<V, E>::operator*()const {
	return element;
}

template <class V, class E>
vertex<V, E>::vertex() 
: element(0), visited(false) {}

template <class V, class E>
vertex<V, E>::vertex(const V& input)
	: element(input), visited(false) {}

//template <class V, class E>
//std::list<edge<V, E>*> vertex<V,E>::incEdges() const {
//	return incList;
//}

template<class V, class E>
bool vertex<V, E>::isAdjacentTo(vertex* otherVert)const {


		for (std::list<edge<V, E>*>::const_iterator i = incList.begin(); i != incList.end(); ++i) {
			if ((*i)->isIncident(otherVert)) {
				return true;
			}
		}
		return false;

}