#pragma once
#include "vertex.h"
#include "edge.h"
#include "other.h"
#include "exceptions.h"
#include <iostream>

template <class V, class E>
class ALgraph {

	template <typename J, typename Q>
	friend std::ostream& operator<< (std::ostream&, const ALgraph<J,Q>&);

public:
	ALgraph();
	ALgraph(const int&, const double&); //!!!
	~ALgraph();
	double density() const;
	int edgeNum() const;
	int verNum() const;
	edge<V,E>* insertEdge( vertex<V, E> *,  vertex<V, E> *, const E&) throw(customEx);
	vertex<V, E>* insertVertex(const V& elem);

	void executeDFS();
	void executeBFS();
	
private: // tools
	vertex<V, E>* randomVertex() const;
	void DFS(vertex<V, E>*);
	std::list<std::list<vertex<V, E>*>*> BFS(vertex<V, E>*);
private: // contents
	std::list<vertex<V, E>*> vList;
	std::list<edge<V, E>*> eList;

};
// CODE BELOW


template <class V, class E>
ALgraph<V,E>::ALgraph() {

	// below example for DFS:
	//vertex<V, E>* v0 = insertVertex(0);
	//vertex<V, E>* v1 = insertVertex(1);
	//vertex<V, E>* v2 = insertVertex(2);
	//vertex<V, E>* v3 = insertVertex(3);
	//vertex<V, E>* v4 = insertVertex(4);

	//edge<V, E>* e1 = insertEdge(v0, v1, 11);
	//edge<V, E>* e2 = insertEdge(v1, v2, 22);
	//edge<V, E>* e3 = insertEdge(v3, v1, 33);
	//edge<V, E>* e4 = insertEdge(v1, v4, 44);
	//edge<V, E>* e5 = insertEdge(v4, v0, 55);

	//below example for BFS:
	vertex<V, E>* v0 = insertVertex(0);
	vertex<V, E>* v1 = insertVertex(1);
	vertex<V, E>* v2 = insertVertex(2);
	vertex<V, E>* v3 = insertVertex(3);
	vertex<V, E>* v4 = insertVertex(4);

	edge<V, E>* e1 = insertEdge(v4, v3, 11);
	edge<V, E>* e2 = insertEdge(v1, v2, 22);
	edge<V, E>* e3 = insertEdge(v0, v1, 33);
	edge<V, E>* e4 = insertEdge(v0, v2, 44);
	edge<V, E>* e5 = insertEdge(v2, v4, 55);
	edge<V, E>* e6 = insertEdge(v1, v3, 66);
}

template <class V, class E> // test ctor here
ALgraph<V, E>::ALgraph(const int& sizeInput, const double& denseInput) {
	try
	{
		if (sizeInput<1 || denseInput < 25.00 || (denseInput >80.00 && denseInput != 100.00))  throw customEx("BAD INPUT!!!! IT'S A DISASTER!!!!!");
		vertex<V, E>* v1ptr = insertVertex(1); // starts with one vert

		//******* achieving desired size
		for (unsigned int i = 0; i < sizeInput - 1; ++i) {
			vertex<V, E>* randomVptr = randomVertex();
			vertex<V, E>* newVptr = insertVertex(rng(0, 100));
			insertEdge(randomVptr,newVptr, 100 + rng(0, 100));
		}
		//*******

		//******* achieving desired density
		while (density() < denseInput) {
			if (denseInput <= 80.00) {
				vertex<V, E>* randomV1 = randomVertex();
				vertex<V, E>* randomV2 = randomVertex();
				if (randomV1 != randomV2 && !(randomV1)->isAdjacentTo(randomV2)) {
					insertEdge(randomV1, randomV2, rng(0, 100) + 100);
				}
			}
			else if (denseInput == 100.00) {
				//connect every vertice to every next vertice
				for (std::list<vertex<V, E>*>::iterator i = vList.begin(); i != vList.end(); ++i) {
					for (std::list<vertex<V, E>*>::iterator j = vList.begin(); j != vList.end(); ++j) {
						if ((*i != *j) && !(*i)->isAdjacentTo(*j)) {
							insertEdge(*i, *j, rng(0, 100) + 100);
						}
					}
				}
			}
		}


		//*******
	}
	catch (customEx& x)
	{
		std::cout << x.what() << std::endl;
	}
}


template <class V, class E>
edge<V,E>* ALgraph<V, E>::insertEdge(vertex<V, E>* v1,vertex<V, E>* v2, const E& elem) throw(customEx) {
	try {
		if (density() >= 100.00) throw customEx("ERROR: insertEdge() - i am at maximum density");
		if (v1 == v2) throw customEx("ERROR: insertEdge() - this is the same vertex");
		edge<V, E>* nEdge = new edge<V, E>(elem);
		nEdge->end1 = v1;
		nEdge->end2 = v2;
		v1->incList.push_back(nEdge);
		v2->incList.push_back(nEdge);
		eList.push_back(nEdge);

		return nEdge;
	}
	catch (customEx& obj) {
		std::cout << obj.what() << std::endl;
	}
}

template<class V, class E>
vertex<V, E>* ALgraph<V, E>::insertVertex(const V& elem) {
	vertex<V, E>* nVert = new vertex<V, E>(elem);
	vList.push_back(nVert);
	return nVert;
}



template <class V, class E>
ALgraph<V,E>::~ALgraph() {

}

template <class V, class E>
double ALgraph<V, E>::density() const {
	if (verNum() != 0 && verNum() - 1 != 0) {
		return 100.00 * 2.00 * (double)edgeNum() / (double)(verNum() * (verNum() - 1.00));
	}
	else {
		return 100.00;
	}
}

template <class V, class E>
int ALgraph<V, E>::edgeNum() const {
	return eList.size();
}
template <class V, class E>
int ALgraph<V, E>::verNum() const {
	return vList.size();
}

template <class V,class E>
std::ostream& operator<<(std::ostream& os, const ALgraph<V,E>& graph) {
	os << "\nFormat:\nWIERZCHOLEK: ((krawedz-sasiad)(krawedz2-sasiad2)...)\n";
	for (std::list<vertex<V, E>*>::const_iterator i = graph.vList.begin(); i != graph.vList.end(); ++i) {
		os << ***i;
		os << ": (";
		for (std::list<edge<V, E>*>::iterator j = (*i)->incList.begin(); j != (*i)->incList.end(); ++j) {
			os << "(" << ***j; // edge element printed here
			if ((*j)->discovery) std::cout << "D";
			if ((*j)->back) std::cout << "B";
			if ((*j)->cross) std::cout << "C";
			std::cout << "-" << **((*j)->opposite(*i)); 
			if (((*j)->opposite(*i))->visited) std::cout << "V"; 
			std::cout << ")";
		}
		os << ")" << std::endl;
	}
	os << "density == " << graph.density() << "%\n";
	os << "num of Vs == " << graph.verNum() << ", num of Es == " << graph.edgeNum() << std::endl;
	return os;
}

template <class V, class E>
vertex<V, E>* ALgraph<V, E>::randomVertex() const {
	if (verNum() == 0) {
		std::cout << "WARNING!!!!!!!! RANDOM VERTEX FAILED!!!!!!!!\n";
		return NULL;
	}
	else {
		int counter = 0;
		int randy = rng(0, verNum() - 1);
		for (std::list<vertex<V, E>*>::const_iterator j = vList.begin(); j != vList.end(); ++j) {
			if (counter == randy) {
				return *j;
				break;
			}
			++counter;
		}
	}
}

template <class V, class E>
void ALgraph<V, E>::DFS(vertex<V, E>* v) {
	v->visited = true;
	vertex<V, E>* vptr;
	for (std::list<edge<V, E>*>::const_iterator i = v->incList.begin(); i != v->incList.end(); ++i) {
		if (!(*i)->back && !(*i)->discovery) {
			vptr = (*i)->opposite(v);
			if (!vptr->visited) {
				(*i)->discovery = true;
				DFS(vptr);
			}
			else {
				(*i)->back = true;
			}
		}
	}
}


template <class V, class E>
void ALgraph<V, E>::executeDFS() {
	DFS(vList.front());
}


template <class V, class E>
std::list<std::list<vertex<V, E>*>*> ALgraph<V, E>::BFS(vertex<V, E>* v) {
	std::list<std::list<vertex<V, E>*>*> level_list;
	std::list<vertex<V, E>*>* list_ptr;
	std::list<vertex<V, E>*>* list_ptr2;
	list_ptr = new std::list<vertex<V, E>*>;
	list_ptr->push_back(v);
	std::list<edge<V, E>*> incEdgeListGuy;
	while (!list_ptr->empty()) {
		list_ptr2 = new std::list<vertex<V, E>*>;
		for (std::list<vertex<V, E>*>::const_iterator j = list_ptr->begin(); j != list_ptr->end(); ++j) {
			incEdgeListGuy = (*j)->incList;
			for (std::list<edge<V, E>*>::const_iterator k = incEdgeListGuy.begin(); k != incEdgeListGuy.end(); ++k) {
				if (!(*k)->discovery && !(*k)->cross) {
					if (!(*k)->opposite(*j)->visited) {
						(*k)->discovery = true;
						list_ptr2->push_back((*k)->opposite(*j));
						(*k)->opposite(*j)->visited = true;
					}
					else {
						(*k)->cross = true;
					}
				}
			}
		}
		level_list.push_back(list_ptr);
		list_ptr = list_ptr2;


	}
	return level_list;
}

template <class V, class E>
void ALgraph<V, E>::executeBFS() {
	std::list<std::list<vertex<V, E>*>*> level_list = BFS(vList.front());


	// below i print the output to test stuff:
	//int ind=0;
	//for (std::list<std::list<vertex<V, E>*>*>::iterator i = level_list.begin(); i != level_list.end(); ++i) {
	//	std::cout << "[" << ind << "] ";
	//	for (std::list<vertex<V, E>*>::iterator j = (*i)->begin(); j != (*i)->end(); ++j)
	//	{
	//		std::cout << ***j << " ";
	//	}
	//	std::cout << std::endl;
	//	++ind;
	//}
}
