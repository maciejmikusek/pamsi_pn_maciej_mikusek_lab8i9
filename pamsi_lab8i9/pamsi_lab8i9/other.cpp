#include "stdafx.h"
#include "other.h"

int rng(const int& beg, const int& end) {
	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<int> dist6(beg, end);

	return dist6(rng);
}

double avgOfArray(const double arr[], int size) {
	double avg = 0.00;
	for (unsigned int i = 0; i < size; ++i) {
		avg += arr[i];
	}
	return avg / size;
}