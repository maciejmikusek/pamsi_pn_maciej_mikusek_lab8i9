#pragma once
#include "Mvertex.h"
#include "exceptions.h"

template <class V, class E>
class Mgraph;

template <class V, class E>
class Mvertex;




template <class V, class E>
class Medge {

	friend class Mgraph<V, E>;
	friend class Mvertex<V, E>;

public:
	Medge();
	Medge(const E&);
	E operator*() const;
	bool isIncident(Mvertex<V, E>*)const;
	Mvertex<V, E>* opposite(Mvertex<V, E>* vert) const throw(customEx);

private:
	E element;
	Mvertex<V, E>* end1;
	Mvertex<V, E>* end2;

public: // DFS flags
	bool discovery;
	bool back;
public: // BFS flag
	bool cross;

};
//CODE BELOW:

template<class V, class E>
E Medge<V, E>::operator*() const {
	return element;
}

template<class V, class E>
Medge<V, E>::Medge()
	: element(0), end1(NULL), end2(NULL), discovery(false), back(false), cross(false) {}

template<class V, class E>
Medge<V, E>::Medge(const E& inp)
	: element(inp), end1(NULL), end2(NULL), discovery(false), back(false), cross(false) {}

template <class V, class E>
bool Medge<V, E>::isIncident(Mvertex<V, E>* aVert) const {
	if (end1 == aVert || end2 == aVert) {
		return true;
	}
	else {
		return false;
	}
}

template<class V, class E>
Mvertex<V, E>* Medge<V, E>::opposite(Mvertex<V, E>* vert) const throw(customEx) {
	try {
		if (!isIncident(vert)) throw customEx("ERROR: opposite(): my arg is not incident!");
		if (end1 == vert) {
			return end2;
		}
		else {
			return end1;
		}

	}
	catch (customEx& obj) {
		std::cout << obj.what() << std::endl;
	}
}