#pragma once
#include <list>
#include "Medge.h"
#include "Mvertex.h"
#include "exceptions.h"


template <class V, class E>
class Mgraph {

	template <typename J, typename Q>
	friend std::ostream& operator<< (std::ostream&, const Mgraph<J, Q>&);


public:
	Mgraph();
	Mgraph(const int&, const double&);
	int edgeNum() const;
	int verNum() const;
	double density() const;
	std::list<Medge<V, E>*> incEdgesOf(Mvertex<V, E>*) const;
	bool VareAdjacent(Mvertex<V, E>*, Mvertex<V, E>*)const throw (customEx);
	bool EareAdjacent(Medge<V, E>*, Medge<V, E>*)const throw(customEx);
	Mvertex<V, E>* insertVertex(const V&);
	Medge<V, E>* insertEdge(Mvertex<V, E>*, Mvertex<V, E>*, const E&) throw(customEx);

	void executeDFS();
	void executeBFS();

private: // contents

	std::list<Mvertex<V, E>*> vList;
	std::list<Medge<V, E>*> eList;
	Medge<V, E>*** matrix;


private: // tools

	Mvertex<V, E>* randomVertex() const;
	void DFS(Mvertex<V, E>*);
	std::list<std::list<Mvertex<V, E>*>*> BFS(Mvertex<V, E>*);


};
// CODE BELOW:

template <class V, class E>
Mgraph<V, E>::Mgraph() // example graph
	: matrix(NULL) {

	// below example for DFS:
	//Mvertex<V, E>* v0 = insertVertex(0);
	//Mvertex<V, E>* v1 = insertVertex(1);
	//Mvertex<V, E>* v2 = insertVertex(2);
	//Mvertex<V, E>* v3 = insertVertex(3);
	//Mvertex<V, E>* v4 = insertVertex(4);

	//Medge<V, E>* e1 = insertEdge(v0, v1, 11);
	//Medge<V, E>* e2 = insertEdge(v1, v2, 22);
	//Medge<V, E>* e3 = insertEdge(v3, v1, 33);
	//Medge<V, E>* e4 = insertEdge(v1, v4, 44);
	//Medge<V, E>* e5 = insertEdge(v4, v0, 55);

	//below example for BFS:
	Mvertex<V, E>* v0 = insertVertex(0);
	Mvertex<V, E>* v1 = insertVertex(1);
	Mvertex<V, E>* v2 = insertVertex(2);
	Mvertex<V, E>* v3 = insertVertex(3);
	Mvertex<V, E>* v4 = insertVertex(4);
	Mvertex<V, E>* v5 = insertVertex(5);
	Mvertex<V, E>* v6 = insertVertex(6);

	Medge<V, E>* e1 = insertEdge(v4, v3, 11);
	Medge<V, E>* e2 = insertEdge(v1, v2, 22);
	Medge<V, E>* e3 = insertEdge(v0, v1, 33);
	Medge<V, E>* e4 = insertEdge(v0, v2, 44);
	Medge<V, E>* e5 = insertEdge(v2, v4, 55);
	Medge<V, E>* e6 = insertEdge(v1, v3, 66);
	Medge<V, E>* e7 = insertEdge(v5, v6, 111);
	Medge<V, E>* e8 = insertEdge(v4, v5, 99);
}

template <class V, class E>
Mgraph<V, E>::Mgraph(const int& sizeInput, const double& denseInput)
	: matrix(NULL) {

	try
	{
		if (sizeInput<1 || denseInput < 25.00 || (denseInput >80.00 && denseInput != 100.00))  
			throw customEx("BAD INPUT!!!! IT'S A DISASTER!!!!!");
		Mvertex<V, E>* v1ptr = insertVertex(1); // starts with one vert

		//******* achieving desired size
		for (unsigned int i = 0; i < sizeInput - 1; ++i) {
			Mvertex<V, E>* randomVptr = randomVertex();
			Mvertex<V, E>* newVptr = insertVertex(rng(0, 100));
			insertEdge(randomVptr, newVptr, 100 + rng(0, 100));
		}
		//*******

		//******* achieving desired density
		while (density() < denseInput) {
			if (denseInput <= 80.00) {
				Mvertex<V, E>* randomV1 = randomVertex();
				Mvertex<V, E>* randomV2 = randomVertex();
				if (randomV1 != randomV2 && !VareAdjacent(randomV1, randomV2)) {
					insertEdge(randomV1, randomV2, rng(0, 100) + 100);
				}
			}
			else if (denseInput == 100.00) {
				//connect every vertice to every next vertice
				for (std::list<Mvertex<V, E>*>::iterator i = vList.begin(); i != vList.end(); ++i) {
					for (std::list<Mvertex<V, E>*>::iterator j = vList.begin(); j != vList.end(); ++j) {
						if ((*i != *j) && !VareAdjacent(*i,*j)) {
							insertEdge(*i, *j, rng(0, 100) + 100);
						}
					}
				}
			}
		}


		//*******
	}
	catch (customEx& x)
	{
		std::cout << x.what() << std::endl;
	}

}

template <class V, class E>
double Mgraph<V, E>::density() const {
	if (verNum() != 0 && verNum() - 1 != 0) {
		return 100.00 * 2.00 * (double)edgeNum() / (double)(verNum() * (verNum() - 1.00));
	}
	else {
		return 100.00;
	}
}

template <class V, class E>
int Mgraph<V, E>::edgeNum() const {
	return eList.size();
}
template <class V, class E>
int Mgraph<V, E>::verNum() const {
	return vList.size();
}

template <class V, class E>
std::list<Medge<V, E>*> Mgraph<V, E>::incEdgesOf(Mvertex<V,E>* vert) const {
	std::list<Medge<V, E>*> listGuy;
	for (unsigned int i = 0; i < verNum(); ++i) {
		if (matrix[vert->index][i] !=NULL)
		listGuy.push_back(matrix[vert->index][i]);
	}
	return listGuy;
}

template <class V, class E>
Mvertex<V, E>* Mgraph<V, E>::randomVertex() const {
	if (verNum() == 0) {
		std::cout << "WARNING!!!!!!!! RANDOM VERTEX FAILED!!!!!!!!\n";
		return NULL;
	}
	else {
		int counter = 0;
		int randy = rng(0, verNum() - 1);
		for (std::list<Mvertex<V, E>*>::const_iterator j = vList.begin(); j != vList.end(); ++j) {
			if (counter == randy) {
				return *j;
				break;
			}
			++counter;
		}
	}
}

template<class V, class E>
bool Mgraph<V, E>::VareAdjacent(Mvertex<V,E>* v1,Mvertex<V,E>* v2)const {
	try
	{
		if (v1 == v2) throw customEx("ERROR: V.adj - this is the same vertice!");
		if (matrix[v1->index][v2->index] != NULL) {
			return true;
		}
		else {
			return false;
		}
	}
	catch (customEx& x)
	{
		std::cout << x.what() << std::endl;
	}
}

template <class V, class E>
Mvertex<V, E>* Mgraph<V, E>::insertVertex(const V& ele) {
	if (matrix == NULL) {
		matrix = new Medge<V, E>**[1];
		matrix[0] = new Medge<V, E>*[1];
		matrix[0][0] = NULL;
	}
	else {
		Medge<V, E>*** temp;
		temp = new Medge<V, E>**[verNum() + 1];
		for (unsigned int i = 0; i < verNum() + 1; ++i) {
			temp[i] = new Medge<V, E>*[verNum() + 1];
		}
		for (unsigned int i = 0; i < verNum()+1; ++i) {
			for (unsigned int j = 0; j < verNum()+1; ++j) {
				temp[i][j] = NULL;
			}
		}
		for (unsigned int i = 0; i < verNum(); ++i) {
			for (unsigned int j = 0; j < verNum(); ++j) {
				temp[i][j] = matrix[i][j];
			}
		}
		for (unsigned int i = 0; i < verNum(); ++i) {
			delete[] matrix[i];
		}
		delete[] matrix;
		matrix = new Medge<V, E>**[verNum() + 1];
		for (unsigned int i = 0; i < verNum() + 1; ++i) {
			matrix[i] = new Medge<V, E>*[verNum() + 1];
		}
		for (unsigned int i = 0; i < verNum()+1; ++i) {
			for (unsigned int j = 0; j < verNum()+1; ++j) {
				matrix[i][j] = temp[i][j];
			}
		}
		for (unsigned int i = 0; i < verNum(); ++i) {
			delete[] temp[i];
		}
		delete[] temp;
	}


	Mvertex<V, E>* newVert = new Mvertex<V, E>(ele, verNum());
	vList.push_back(newVert);
	return newVert;
}

template <class V, class E>
std::ostream& operator<<(std::ostream& os, const Mgraph<V, E>& graph) {
	os << "\nFormat:\n[index] WIERZCHOLEK: ((krawedz-sasiad)(krawedz2-sasiad2)...)\n";
	std::list<Medge<V, E>*>	incEdgeList;
	for (std::list<Mvertex<V, E>*>::const_iterator i = graph.vList.begin(); i != graph.vList.end(); ++i) {
		os << "[" << (*i)->gindex() << "] ";
		os << ***i;
		os << ": (";
		incEdgeList = graph.incEdgesOf(*i);
		for (std::list<Medge<V, E>*>::const_iterator j = incEdgeList.begin(); j != incEdgeList.end(); ++j) {
			os << "(" << ***j; // edge element printed here
			if ((*j)->discovery) std::cout << "D";
			if ((*j)->back) std::cout << "B";
			if ((*j)->cross) std::cout << "C";
			std::cout << "-" << **((*j)->opposite(*i)); // vertice element printed here
			if (((*j)->opposite(*i))->visited) std::cout << "V";
			std::cout<< ")";
		}
		os << ")" << std::endl;
	}
	os << "density == " << graph.density() << "%\n";
	os << "num of Vs == " << graph.verNum() << ", num of Es == " << graph.edgeNum() << std::endl;
	return os;
}

template <class V, class E>
Medge<V, E>* Mgraph<V, E>::insertEdge(Mvertex<V, E>* v1, Mvertex<V, E>* v2, const E& elem) throw(customEx) {
	try {
		if (density() >= 100.00) throw customEx("ERROR: insertEdge() - i am at maximum density");
		if (v1 == v2) throw customEx("ERROR: insertEdge() - this is the same vertex");
		Medge<V, E>* nEdge = new Medge<V, E>(elem);

		nEdge->end1 = v1;
		nEdge->end2 = v2;

		matrix[v1->index][v2->index] = nEdge;
		matrix[v2->index][v1->index] = nEdge;


		eList.push_back(nEdge);
		return nEdge;
	}
	catch (customEx& obj) {
		std::cout << obj.what() << std::endl;
	}
}

template<class V, class E>
bool Mgraph<V, E>::EareAdjacent(Medge<V, E>* e1, Medge<V, E>* e2)const throw (customEx) {
	try
	{
		if (e1 == e2) throw customEx("ERROR: E.adj - this is the same edge!");
		std::list<Medge<V, E>*> incEdgeList;
		incEdgeList = incEdgesOf(e1->end1);
		for (std::list<Medge<V, E>*>::const_iterator j = incEdgeList.begin(); j != incEdgeList.end(); ++j) {
			if (*j == e2) {
				return true;
			}
		}
		incEdgeList = incEdgesOf(e1->end2);
		for (std::list<Medge<V, E>*>::const_iterator j = incEdgeList.begin(); j != incEdgeList.end(); ++j) {
			if (*j == e2) {
				return true;
			}
		}

		return false;
	}
	catch (customEx& x)
	{
		std::cout << x.what() << std::endl;
	}
}

template <class V, class E>
void Mgraph<V,E>::DFS(Mvertex<V, E>* v) {
	std::cout << "Odwiedzam wezel " << **v << "\n";
	v->visited = true;
	Mvertex<V, E>* vptr;
	std::list<Medge<V, E>*>	incEdgeList = incEdgesOf(v);
	for (std::list<Medge<V, E>*>::const_iterator i = incEdgeList.begin(); i != incEdgeList.end(); ++i) {
		if (!(*i)->back && !(*i)->discovery) {
			vptr = (*i)->opposite(v);
			if (!vptr->visited) {
				(*i)->discovery = true;
				std::cout << "Odwiedzam krawedz " << ***i << " - discovery\n";
				DFS(vptr);
			}
			else {
				(*i)->back = true;
				std::cout << "Odwiedzam krawedz " << ***i << " - back\n";
			}
		}
	}
}

template <class V, class E>
void Mgraph<V, E>::executeDFS() {
	DFS(vList.front());
}

template <class V, class E>
std::list<std::list<Mvertex<V,E>*>*> Mgraph<V, E>::BFS(Mvertex<V, E>* v) {
	std::list<std::list<Mvertex<V, E>*>*> level_list;
	std::list<Mvertex<V, E>*>* list_ptr;
	std::list<Mvertex<V, E>*>* list_ptr2;
	list_ptr = new std::list<Mvertex<V, E>*>;
	list_ptr->push_back(v);
	std::list<Medge<V, E>*> incEdgeListGuy;
	while (!list_ptr->empty()) {
		list_ptr2 = new std::list<Mvertex<V, E>*>;
		for (std::list<Mvertex<V, E>*>::const_iterator j = list_ptr->begin(); j != list_ptr->end(); ++j) {
			incEdgeListGuy = incEdgesOf(*j);
			for (std::list<Medge<V, E>*>::const_iterator k = incEdgeListGuy.begin(); k != incEdgeListGuy.end(); ++k) {
				if (!(*k)->discovery && !(*k)->cross) {
					if (!(*k)->opposite(*j)->visited) {
						(*k)->discovery = true;
						list_ptr2->push_back((*k)->opposite(*j));
						(*k)->opposite(*j)->visited = true;
					}
					else {
						(*k)->cross = true;
					}
				}
			}
		}
		level_list.push_back(list_ptr);
		list_ptr = list_ptr2;


	}
	return level_list;
}

template <class V, class E>
void Mgraph<V, E>::executeBFS() {
	std::list<std::list<Mvertex<V, E>*>*> level_list = BFS(vList.front());


	// below i print the output to test stuff:
	int ind=0;
	for (std::list<std::list<Mvertex<V, E>*>*>::iterator i = level_list.begin(); i != level_list.end(); ++i) {
		std::cout << "[" << ind << "] ";
		for (std::list<Mvertex<V, E>*>::iterator j = (*i)->begin(); j != (*i)->end(); ++j)
		{
			std::cout << ***j << " ";
		}
		std::cout << std::endl;
		++ind;
	}
}