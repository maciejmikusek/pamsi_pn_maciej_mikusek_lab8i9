#pragma once
#include "Medge.h"
#include "exceptions.h"

template <class V, class E>
class Mgraph;

template <class V, class E>
class Medge;

template <class V, class E>
class Mvertex {
	
	friend class Mgraph<V, E>;
	friend class Medge<V, E>;

public:
	Mvertex();
	Mvertex(const V&, const unsigned int&);
	V operator*() const;



	int gindex() const { return index; }
private:
	V element;
	unsigned int index;
public: // DFS flags
	bool visited;

};
// code:



template <class V, class E>
Mvertex<V, E>::Mvertex()
	: element(0), index(0), visited(false) {}

template <class V, class E>
Mvertex<V, E>::Mvertex(const V& input, const unsigned int& ind)
	: element(input), index(ind), visited(false) {}


template <class V, class E>
V Mvertex<V, E>::operator*()const {
	return element;
}

